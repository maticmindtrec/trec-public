# Fastlane common configurations for TreC apps

To include the Fastlane common configuration, add the following to you Fastfile:

```ruby
#Android
import_from_git(
  url: "git@gitlab.fbk.eu:trec-public/common-config.git",
  branch: "master", # The branch to checkout on the repository
  path: "apps/fastlane/android.rb"
)
# iOS
import_from_git(
  url: "git@gitlab.fbk.eu:trec-public/common-config.git",
  branch: "master", # The branch to checkout on the repository
  path: "apps/fastlane/ios.rb"
)
```

Define the variables specific for you app (team id and app id) using the `Appfile` in your project.

# Fastlane lanes for building Android application and deploy to Play Store

platform :android do

	# APKS #####################################################################

	desc "Build the APK file"
	lane :build do |options|
		if not options[:flavor]
			UI.abort_with_message! "Add flavor parameter to this command in the format `flavor:development` (or staging, production)"
		end
		gradle(
			task: 'assemble',
			build_type: 'Release',
			flavor: options[:flavor],
			project_dir: './android',

			# Set some gradle properties to sign the application
			properties: {
				"android.injected.signing.store.file" => ENV["ANDROID_KEYSTORE_FILE"],
				"android.injected.signing.store.password" => ENV["ANDROID_KEYSTORE_PASSWORD"],
				"android.injected.signing.key.alias" => ENV["ANDROID_KEYSTORE_ALIAS"],
				"android.injected.signing.key.password" => ENV["ANDROID_KEYSTORE_ALIAS_PASSWORD"],
			}
		)
	end

	desc "Deploy to Google Play internal testing"
	lane :deploy_internal_test do
		upload_to_play_store(
			track: 'internal',
			apk: 'android/app/build/outputs/apk/staging/release/app-staging-release.apk'
		)
	end

	desc "Deploy to Google Play production"
	lane :deploy_production do
		upload_to_play_store(
			track: 'production',
			apk: 'android/app/build/outputs/apk/production/release/app-production-release.apk'
		)
	end


	# APP BUNDLE ###############################################################

	desc "Build the App Bundle"
	lane :build_app_bundle do |options|
		if not options[:flavor]
			UI.abort_with_message! "Add flavor parameter to this command in the format `flavor:development` (or staging, production)"
		end

		# Extract keystore version
		KEYSTORE_VERSION = options[:keystore_version]
		UI.message "Using keystore version #{KEYSTORE_VERSION.empty? ? "V1" : "V#{KEYSTORE_VERSION}"}"

		# Get version code/name
		VERSION_CODE = ENV["APP_ANDROID_VERSION_CODE"]
    	VERSION_NAME = ENV["APP_VERSION_COMPLETE"]

		# Set some gradle properties to sign the application
		properties = {
			"android.injected.signing.store.file" => ENV["ANDROID_KEYSTORE_FILE"],
			"android.injected.signing.store.password" => ENV["ANDROID_KEYSTORE_PASSWORD#{!KEYSTORE_VERSION.empty? ? "_V#{KEYSTORE_VERSION}" : ""}"],
			"android.injected.signing.key.alias" => ENV["ANDROID_KEYSTORE_ALIAS#{!KEYSTORE_VERSION.empty? ? "_V#{KEYSTORE_VERSION}" : ""}"],
			"android.injected.signing.key.password" => ENV["ANDROID_KEYSTORE_ALIAS_PASSWORD#{!KEYSTORE_VERSION.empty? ? "_V#{KEYSTORE_VERSION}" : ""}"],
		}

		# Add additional properties from env
		if VERSION_CODE
			properties["android.injected.version.code"] = VERSION_CODE
		end

		if VERSION_NAME
			properties["android.injected.version.name"] = VERSION_NAME
		end

		gradle(
			task: 'bundle',
			build_type: 'Release',
			flavor: options[:flavor],
			project_dir: './android',
			properties: properties,
			print_command: false
		)
	end

	desc "Deploy to Google Play internal testing (App bundle)"
	lane :deploy_internal_test_app_bundle do
		upload_to_play_store(
			track: 'internal',
			aab: 'android/app/build/outputs/bundle/stagingRelease/app-staging-release.aab'
		)
	end

	desc "Deploy to Google Play production (App bundle)"
	lane :deploy_production_app_bundle do
		upload_to_play_store(
			track: 'production',
			aab: 'android/app/build/outputs/bundle/productionRelease/app-production-release.aab'
		)
	end

end

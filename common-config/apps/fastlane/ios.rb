# Fastlane lanes for building iOS application and deploy to App Store

SLACK_URL = "https://hooks.slack.com/services/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"

platform :ios do

	desc "Check the development provisioning profile and key for IOS, and create them if necessary"
	lane :provision_development do |options|
		org = options[:org]
		KEY_ID = "FASTLANE_#{org}_KEY_ID"
		ISSUER_ID = "FASTLANE_#{org}_ISSUER_ID"
		KEY_CONTENT = "FASTLANE_#{org}_KEY_CONTENT"

		api_key = app_store_connect_api_key(
			key_id: "#{ENV[KEY_ID]}",
			issuer_id: "#{ENV[ISSUER_ID]}",
			key_content: "#{ENV[KEY_CONTENT]}",
			duration: 1200, # optional (maximum 1200)
			in_house: false, # states that it is App Store and not Enterprise (in house)
		)

		match(
			api_key: api_key,
			type: "development",
			force_for_new_devices: true,				# Update profile if the device count changes in the online portal
			force: options[:force]? options[:force]: false,
			readonly: options[:readonly]? true: is_ci
		)
	end

	desc "Check the production provisioning profile and key for iOS, and create them if necessary"
	lane :provision_production do |options|
		org = options[:org]
		KEY_ID = "FASTLANE_#{org}_KEY_ID"
		ISSUER_ID = "FASTLANE_#{org}_ISSUER_ID"
		KEY_CONTENT = "FASTLANE_#{org}_KEY_CONTENT"

		api_key = app_store_connect_api_key(
			key_id: "#{ENV[KEY_ID]}",
			issuer_id: "#{ENV[ISSUER_ID]}",
			key_content: "#{ENV[KEY_CONTENT]}",
			duration: 1200, # optional (maximum 1200)
			in_house: false, # states that it is App Store and not Enterprise (in house)
		)

		match(
			api_key: api_key,
			type: "appstore",
			readonly: options[:readonly]? true : is_ci
		)
	end

	desc "Create APN certificates for push notifications and save them locally"
	lane :'apn_certificate' do |options|

		if not options[:environment]
			UI.abort_with_message! "Add development parameter to this command in the format `environment:development` (or production)"
		end

		get_push_certificate(
			# force: true, # create a new profile, even if the old one is still valid
			development: options[:environment] == "development" ? true : false,
			save_private_key: true,
			new_profile: proc do |profile_path| # this block gets called when a new profile was generated
				puts profile_path # the absolute path to the new PEM file
				# insert the code to upload the PEM file to the server
			end
		)
	end

	desc "Package an emulator version of the app for the emulator"
	lane :build_emulator do |options|

		if not options[:scheme]
			UI.abort_with_message! "Add scheme parameter to this command in the format `scheme:Development` (or Staging, Production)"
		end

		build_app(
			workspace: './ios/App/App.xcworkspace/',
			skip_package_ipa: true,
			build_path: "emulator",
			scheme: options[:scheme],
			export_method: options[:scheme] == 'Development' ? 'development' : 'app-store',
			destination: "generic/platform=iOS Simulator"
		)
	end

	desc "Package a development version of the app, signing with development certificates"
	lane :build_development do
		provision_development org:ENV["APP_STORE_CONNECT_ORG"]
		increment_build_number(
			build_number: ENV['CI_JOB_ID'],
			xcodeproj: './ios/App/App.xcodeproj/'
		)
		build_app(
			workspace: './ios/App/App.xcworkspace/',
			export_method: 'development',
			scheme: 'Development'
		)
	end

	desc "Package a production version of the app, using the Git tag and GitLab job ID for the version and build numbers"
	lane :build do |options|

		if not options[:scheme]
			UI.abort_with_message! "Add scheme parameter to this command in the format `scheme:Development` (or Staging, Production)"
		end

		provision_production org:ENV["APP_STORE_CONNECT_ORG"]
		increment_version_number(
			version_number: /\d+\.\d+\.\d+/.match(ENV['CI_COMMIT_TAG'])[0], # Read git version tag such as v0.1.0, and remove v char and staging
			xcodeproj: './ios/App/App.xcodeproj/'
		)
		increment_build_number(
			build_number: ENV['CI_JOB_ID'],
			xcodeproj: './ios/App/App.xcodeproj/'
		)
		build_app(
			workspace: './ios/App/App.xcworkspace/',
			scheme: options[:scheme],
			output_name: "App-#{ENV['CI_COMMIT_TAG']}.ipa"
		)
	end

	desc "Deploy to App Store Testflight"
	lane :deploy_testflight do |options|
		if not options[:org]
			UI.abort_with_message! "App Store Connect organization is not defined. Please set it using APP_STORE_CONNECT_ORG environment variable."
		else
			UI.message "Upload to Testflight using App Store Connect API Key"
			org = options[:org]
			KEY_ID = "FASTLANE_#{org}_KEY_ID"
			ISSUER_ID = "FASTLANE_#{org}_ISSUER_ID"
			KEY_CONTENT = "FASTLANE_#{org}_KEY_CONTENT"

			api_key = app_store_connect_api_key(
				key_id: "#{ENV[KEY_ID]}",
				issuer_id: "#{ENV[ISSUER_ID]}",
				key_content: "#{ENV[KEY_CONTENT]}",
				duration: 1200, # optional (maximum 1200)
				in_house: false, # states that it is App Store and not Enterprise (in house)
			)

			begin
				upload_to_testflight(
					api_key: api_key,
					ipa: "App-#{ENV['CI_COMMIT_TAG']}.ipa",
					reject_build_waiting_for_review: true
				)

				slack(
					message: "App successfully uploaded to TestFlight.",
					success: true,
					slack_url: SLACK_URL,
					default_payloads: [:git_branch, :last_git_commit_message],
					attachment_properties: {
						fields: [
							{
								title: "Project",
								value: ENV["CI_PROJECT_NAME"]
							},
							{
								title: "Tag",
								value: ENV["CI_COMMIT_TAG"]
							},
						]
					}
				)
			rescue => exception
				slack(
					message: "Error occured!",
					success: false,
					slack_url: SLACK_URL,
					attachment_properties: {
						fields: [
							{
								title: "Project",
								value: ENV["CI_PROJECT_NAME"]
							},
							{
								title: "Tag",
								value: ENV["CI_COMMIT_TAG"]
							},
							{
								title: "Error message",
								value: exception
							}
						]
					}
				)

				UI.abort_with_message! "#{exception}"
			end

		end
	end

	desc "Deploy to App Store. Searches metadata files to upload inside fastlane/metadata/ios"
	lane :deploy_production do |options|
		if not options[:org]
			UI.abort_with_message! "App Store Connect organization is not defined. Please set it using APP_STORE_CONNECT_ORG environment variable."
		else
			UI.message "Upload to App Store using App Store Connect API Key"
			org = options[:org]
			KEY_ID = "FASTLANE_#{org}_KEY_ID"
			ISSUER_ID = "FASTLANE_#{org}_ISSUER_ID"
			KEY_CONTENT = "FASTLANE_#{org}_KEY_CONTENT"

			api_key = app_store_connect_api_key(
				key_id: "#{ENV[KEY_ID]}",
				issuer_id: "#{ENV[ISSUER_ID]}",
				key_content: "#{ENV[KEY_CONTENT]}",
				duration: 1200, # optional (maximum 1200)
				in_house: false, # states that it is App Store and not Enterprise (in house)
			)

			begin
				deliver(
					api_key: api_key,
					ipa: "App-#{ENV['CI_COMMIT_TAG']}.ipa",
					metadata_path: './fastlane/metadata/ios',
					submit_for_review: true,
					automatic_release: true,
					reject_if_possible: true,
					submission_information: {
						export_compliance_uses_encryption: true,
						export_compliance_is_exempt: true,
						add_id_info_uses_idfa: false,
						include_in_app_purchases: false
					},
					ignore_language_directory_validation: true,
					force: true,   # Disable HTML report generated by Fastlane
					precheck_include_in_app_purchases: false,
				)

				slack(
					message: "App successfully uploaded to App Store Connect and submitted for review.",
					success: true,
					slack_url: SLACK_URL,
					default_payloads: [:git_branch, :last_git_commit_message],
					attachment_properties: {
						fields: [
							{
								title: "Project",
								value: ENV["CI_PROJECT_NAME"]
							},
							{
								title: "Tag",
								value: ENV["CI_COMMIT_TAG"]
							},
						]
					}
				)
			rescue => exception
				slack(
					message: "Error occured!",
					success: false,
					slack_url: SLACK_URL,
					attachment_properties: {
						fields: [
							{
								title: "Project",
								value: ENV["CI_PROJECT_NAME"]
							},
							{
								title: "Tag",
								value: ENV["CI_COMMIT_TAG"]
							},
							{
								title: "Error message",
								value: exception
							}
						]
					}
				)

				UI.abort_with_message! "#{exception}"
			end

		end
	end

end

#!/bin/bash

# This script will read the env variables st by Gitlab about the current Git tag,
# and use it to export some other variables used during the Xcode build
# to set the version number and build number in the IOS app

# The Git tag is used for the version number, while the Gitlab internal Job id for the build number

set -o errexit
set -o pipefail
set -o nounset

# Use the Gitlab internal ID for the build number
export BUILD_NUMBER=$CI_JOB_ID

# Regex to match against the Git tag
regex="ios-v([0-9]+)\.([0-9]+)\.([0-9]+)"

if [[ ${CI_COMMIT_TAG:-} =~ $regex ]]
then

  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  patch="${BASH_REMATCH[3]}"

  export VERSION_NUMBER="$major.$minor.$patch"

  echo "The application will have the version number $VERSION_NUMBER and build number $BUILD_NUMBER"
else
  echo "WARNING: There was a problem in reading the version number from the git tag. Ensure it is in the format ios-v1.0.1"
  echo "The application will use version number: 0.0.0 and build number: $BUILD_NUMBER"
  export VERSION_NUMBER="0.0.0"
fi

# Auto-generate a client from the openapi/swagger definition exposed at runtime by the microservice
# This jobs download the openapi-generator, use it to generate the required classes and publish the client to a remote repository
# To generate the client we need to reach the microservice openapi definition,
# so this job is executed after having deployed the microservice in the server

.client_rest_common: &client_rest_common
  stage: client publish
  variables:
    OPENAPI_GENERATOR_VERSION: 5.1.1
    TREC_SERVER_BASE_URL: https://trec-staging.fbk.eu
  before_script:
    - wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/$OPENAPI_GENERATOR_VERSION/openapi-generator-cli-$OPENAPI_GENERATOR_VERSION.jar -O /tmp/openapi-generator-cli.jar
  script: &client_rest_common_script
    - >-
      java
      ${JAVA_OPTS}
      -jar /tmp/openapi-generator-cli.jar
      generate
      -g $OG_GENERATOR_NAME
      -i $TREC_SERVER_BASE_URL/api/$CI_PROJECT_NAME/swagger.json
      -c $OG_CONFIG_PATH
      -o $OG_OUTPUT_PATH
      --skip-validate-spec
      --global-property apiTests=false,modelTests=false,skipFormModel=false
  dependencies: [] # No dependencies from the previous stages
  artifacts:
    expire_in: 7 days
    paths:
      - client/

# Java client -------------------------------------------------------------------------------------

# Client REST Java Publish
#
# Auto-generate the OpenAPI REST client and publish it
client rest Java publish:
  extends: .client_rest_common
  variables:
    OG_GENERATOR_NAME: java
    OG_OUTPUT_PATH: client/rest
    OG_CONFIG_PATH: client/rest/openapi-settings.yaml
  script:
    - *client_rest_common_script
    - ./gradlew ${CI_PROJECT_NAME}-client-rest:shadowJar # Create the fatjar to publish
    - ./gradlew "${CI_PROJECT_NAME}-client-rest:publish"
  only:
    - /^v\d{1,2}\.\d{1,2}\.\d{1,2}$/ # Matches version tags (ex v1.0.2)

# Client REST Java Build
#
# Auto generate the REST client of a microservice using the openapi-generator
# and try to build it (without publishing)
client rest Java build:
  extends: .client_rest_common
  variables:
    OG_GENERATOR_NAME: java
    OG_OUTPUT_PATH: client/rest
    OG_CONFIG_PATH: client/rest/openapi-settings.yaml
  script:
    - *client_rest_common_script
    - ./gradlew ${CI_PROJECT_NAME}-client-rest:shadowJar # Create the fatjar to publish
    - ./gradlew "${CI_PROJECT_NAME}-client-rest:compileDependencies"
    - ./gradlew "${CI_PROJECT_NAME}-client-rest:compileJava"
  when: manual
  needs: [] # Can be run before jobs in prev stages

# JS client ---------------------------------------------------------------------------------------

#
# Ensure an openapi config for the JS client exists.
# If it does not exists, create a default one
#
.client_rest_js_config:
  script: &client_rest_js_config_script
    - |
      echo '
      #!/usr/bin/env sh

      if [ -f "$OG_CONFIG_PATH" ]; then
        echo "OpenAPI config exists in project ($OG_CONFIG_PATH). Use it."
      else
        echo "OpenAPI config DOES NOT exist in project ($OG_CONFIG_PATH). Create a default one."
        mkdir -p "$(dirname "$OG_CONFIG_PATH")"
        echo -e "npmName: \"@trec/client-$CI_PROJECT_NAME\"\nprefixParameterInterfaces: true\n" > $OG_CONFIG_PATH
      fi
      ' > /tmp/client_rest_js_config_script.sh
    - chmod +x /tmp/client_rest_js_config_script.sh
    - /tmp/client_rest_js_config_script.sh

# Client REST JS Publish
#
# Auto-generate the OpenAPI REST client and publish it
client rest JS publish:
  extends: .client_rest_common
  variables:
    OG_GENERATOR_NAME: typescript-fetch
    OG_OUTPUT_PATH: client/js
    OG_CONFIG_PATH: client/js/openapi-settings.yaml
  script:
    - apk add nodejs npm # Install node on-the-fly
    - *client_rest_js_config_script # Ensure OpenAPI config exists
    - *client_rest_common_script
    - cd $OG_OUTPUT_PATH
    - npm version $CI_COMMIT_TAG
    - npm i
    # Setup NPM config
    - npm set --userconfig .npmrc @trec:registry=${NPM_TREC_REGISTRY_URL}
    - npm set --userconfig .npmrc $NPM_TREC_REGISTRY_USERNAME
    - npm set --userconfig .npmrc $NPM_TREC_REGISTRY_PASSWORD
    - npm set --userconfig .npmrc $NPM_TREC_REGISTRY_EMAIL
    - npm publish
  only:
    - /^v\d{1,2}\.\d{1,2}\.\d{1,2}$/ # Matches version tags (ex v1.0.2)

# Client REST JS Build
#
# Auto generate the REST client of a microservice using the openapi-generator
# and try to build it (without publishing)
client rest JS build:
  extends: .client_rest_common
  variables:
    OG_GENERATOR_NAME: typescript-fetch
    OG_OUTPUT_PATH: client/js
    OG_CONFIG_PATH: client/js/openapi-settings.yaml
  script:
    - apk add nodejs npm # Install node on-the-fly
    - *client_rest_js_config_script # Ensure OpenAPI config exists
    - *client_rest_common_script
    - cd $OG_OUTPUT_PATH
    - npm i
  when: manual
  needs: [] # Can be run before jobs in prev stages

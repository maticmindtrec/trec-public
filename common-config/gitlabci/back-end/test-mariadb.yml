# Execute tests against MariaDB database
# Redis and RabbitMQ are also started as part of the test

include:
  - project: "trec-public/common-config"
    file: "/gitlabci/back-end/common-jobs-test.yml"

# Define mariadb test job anchor
.mariadb:
  services:
    - name: mariadb:10.2
      command: ["--character-set-server=utf8mb4",  "--collation-server=utf8mb4_unicode_ci"]   # Set default server charset to UTF8
    - redis:5-alpine
    - name: rabbitmq:3.7-management-alpine
      alias: rabbitmq
    - name: minio/minio
      alias: minio
      command: ["server", "/data"]
  variables:
    # DB configuration
    MYSQL_DATABASE: trec
    MYSQL_USER: username
    MYSQL_PASSWORD: password
    MYSQL_ROOT_PASSWORD: rootPassword
    # Microservice configuration for connecting to DB
    DB_URL: mariadb://mariadb:3306/trec?allowMultiQueries=true      # Add JDBC parameter, used while dropping the DB during the tests
    DB_USERNAME: username
    DB_PASSWORD: password
    DB_CONNECTION_POOL_SIZE: "5"
    SPRING_DATASOURCE_URL: "jdbc:mariadb://mariadb:3306/trec?allowMultiQueries=true"
    SPRING_DATASOURCE_USERNAME: "username"
    SPRING_DATASOURCE_PASSWORD: "password"
    # Microservice configuration for connecting to RabbitMQ
    RABBITMQ_HOST: "rabbitmq"
    RABBITMQ_USERNAME: guest
    RABBITMQ_PASSWORD: guest
    SPRING_RABBITMQ_HOST: "rabbitmq"
    SPRING_RABBITMQ_USERNAME: guest
    SPRING_RABBITMQ_PASSWORD: guest
    # Microservice configuration for connecting to REDIS
    REDIS_URL: redis
    REDIS_PORT: "6379"
    SPRING_REDIS_HOST: "redis"
    SPRING_REDIS_PORT: "6379"
  before_script:
    - "./gradlew waitDB"
    - "./gradlew waitRedis"
    - "./gradlew compileDependencies --configuration compileClasspath | grep -e 'eu.fbk.trec\\|Project'"
  script:
    - "./gradlew check --stacktrace"
    - "./gradlew printCoverage"
  dependencies: []            # Avoid downloading dependencies from previous jobs
  artifacts: !reference [.test_artifacts, artifacts]
  coverage: !reference [.test_artifacts, coverage]
  interruptible: true
######################################################

# Execute tests on master branch and version tags
mariadb:
  extends: .mariadb
  stage: test
  only:
    # Execute tests only if we are on master branch or there is a version tag
    - master
    - /^v\d{1,2}\.\d{1,2}\.\d{1,2}$/    # Matches version tags (ex v1.0.2)

# Execute tests after deploying to development environment
mariadb development:
  extends: .mariadb
  stage: test development
  needs: []
  when: manual
  allow_failure: false
  except:
    # Execute tests when we are not on master branch or version tag
    - master
    - /^v\d{1,2}\.\d{1,2}\.\d{1,2}$/    # Matches version tags (ex v1.0.2)

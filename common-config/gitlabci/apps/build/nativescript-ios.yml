variables:
  NATIVESCRIPT_IMAGE_VERSION: latest
  NATIVESCRIPT_CLI_VERSION: "6.2.0"

.ios_artifacts: &ios_artifacts
  expire_in: "7 days"
  paths:
    - platforms/ios/build/Release-iphoneos/*.ipa
    - platforms/ios/*/app
    - sourceMap
    - report

.ios_build_template: &ios_build_template
  image: trecregistry.azurecr.io/trec/tools/apps-development-container
  dependencies: []                                        # Important: avoid downloading dependencies from previous stages!
  variables:
    NPM_CONFIG_CACHE: "~/npm-cache/"        # Use NPM cache folder in user directory because we are outside Docker
    NODE_OPTIONS: "--max-old-space-size=8096"
  before_script:
   # Install Nativescript CLI (useful when updating Nativescript framework to newer version)
    - npm install --global --unsafe-perm nativescript@$NATIVESCRIPT_CLI_VERSION
    # Install project dependencies
    - npm ci
    # Download script for settings build version
    - curl -o ios-set-build-version.sh https://gitlab.com/maticmindtrec/trec-public/-/raw/main/common-config/apps/ios-set-build-version.sh
    - chmod +x ios-set-build-version.sh
    - source ./ios-set-build-version.sh        # Read environment variables to set the build and version number
  artifacts: *ios_artifacts
  only:
    - /^ios-v\d{1,2}\.\d{1,2}\.\d{1,2}$/    # Matches version tags (ex ios-v1.0.2)
  tags:
    - ios

################################################################################

iOS Build Development:
  <<: *ios_build_template
  stage: Package Development
  script:
    - fastlane match development --readonly=false --force_for_new_devices             # Install development provisioning profile
    - tns build ios --bundle --env.uglify --env.aot --env.report --env.ci --for-device --env.sourceMap --env.hiddenSourceMap --provision "match Development $APP_ID"
  except:
    - tags
  only:
    - branches

iOS Build Staging:
  <<: *ios_build_template
  stage: Package Staging
  script:
    - fastlane match appstore --readonly=false
    - mv docs/google-config/staging/GoogleService-Info.plist app/App_Resources/iOS/GoogleService-Info.plist || true # Replace Google services configuration with the "fbk-staging" one
    - tns build ios --bundle --env.uglify --env.aot --env.report --env.ci --for-device --env.sourceMap --env.hiddenSourceMap --release --env.trec_env_staging --provision "match AppStore $APP_ID"
  # Avoid this job if variable SKIP_STAGING is defined in the pipeline
  except:
    variables:
      - $SKIP_STAGING

iOS Build Production:
  <<: *ios_build_template
  stage: Package Production
  script:
    - fastlane match appstore --readonly=false
    - mv docs/google-config/production/GoogleService-Info.plist app/App_Resources/iOS/GoogleService-Info.plist || true # Replace Google services configuration with the "fbk-production" one
    - tns build ios --bundle --env.uglify --env.aot --env.report --env.ci --for-device --env.sourceMap --env.hiddenSourceMap --release --env.trec_env_production --provision "match AppStore $APP_ID"
  when: manual          # The release in Production track must be triggered manually
  allow_failure: false

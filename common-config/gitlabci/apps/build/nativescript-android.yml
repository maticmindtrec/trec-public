variables:
  NATIVESCRIPT_IMAGE_VERSION: latest

# Artifacts to be uploaded to GitLab after a build
.android_artifacts: &android_artifacts
  expire_in: "7 days"
  paths:
    - platforms/android/app/build/outputs/apk
    - platforms/android/app/src/main/assets/app
    - sourceMap
    - report
    - appbundle.aab

# Template used by all Android jobs, it installs all the required Android SDK components
.android_build_template: &android_build_template
  image: trecregistry.azurecr.io/trec/tools/apps-development-container:${NATIVESCRIPT_IMAGE_VERSION}
  dependencies: [] # Important: avoid downloading dependencies from previous stages!
  variables: &android_variables
    NODE_OPTIONS: "--max-old-space-size=8096"
    ANDROID_COMPILE_SDK: "28"
    ANDROID_BUILD_TOOLS: "28.0.2"
    ANDROID_SDK_TOOLS: "4333796"
    ANDROID_SDK_DIR: "/cache/sdk" # Install Android SDK in the /cache folder, that is persisted between builds
    ANDROID_HOME: /cache/sdk/android-sdk-linux
  before_script:
    # Install Android SDK
    - apt-get --quiet update --yes
    - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
    - mkdir -p $ANDROID_SDK_DIR
    - wget --quiet --timestamping --directory-prefix=$ANDROID_SDK_DIR https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
    - unzip -u -q -d $ANDROID_SDK_DIR/android-sdk-linux $ANDROID_SDK_DIR/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
    - echo y | $ANDROID_SDK_DIR/android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
    - echo y | $ANDROID_SDK_DIR/android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
    - echo y | $ANDROID_SDK_DIR/android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
    # temporarily disable checking for EPIPE error and use yes to accept all licenses
    - set +o pipefail
    - yes | $ANDROID_SDK_DIR/android-sdk-linux/tools/bin/sdkmanager --licenses
    - set -o pipefail
    # Install project dependencies in package.json
    - npm ci
  artifacts: *android_artifacts
  only:
    - /^android-v\d{1,2}\.\d{1,2}\.\d{1,2}$/ # Matches version tags (ex android-v1.0.2)

#########################################################################################

# Build Android application (pointing to TreC development environment)
Android Build Development:
  <<: *android_build_template
  stage: Package Development
  variables:
    <<: *android_variables
    BUILD_ENVIRONMENT: development
  script:
    - >-
      tns build android
      --bundle
      --env.uglify
      --env.aot
      --env.report
      --env.ci
      --env.sourceMap --env.hiddenSourceMap
  except:
    - tags
  only:
    - branches

# Build Android application for release in internal test track (pointing to staging environment) and sign it using the Android keystore
Android Build Staging:
  <<: *android_build_template
  stage: Package Staging
  variables:
    <<: *android_variables
    BUILD_ENVIRONMENT: staging
  script:
    - echo $ANDROID_KEYSTORE | base64 -di > /tmp/keystore # Copy keystore file to temp file
    - mv docs/google-config/staging/google-services.json app/App_Resources/Android/google-services.json || true # Replace Google services configuration with the "fbk-staging" one
    # Multiline YAML string, it is collapsed in one line when parsed by Gitlab
    - >-
      tns build android
      --bundle
      --env.uglify
      --env.aot
      --env.report
      --env.ci
      --env.sourceMap --env.hiddenSourceMap
      --release
      --key-store-path /tmp/keystore
      --key-store-password $ANDROID_KEYSTORE_PASSWORD --key-store-alias $ANDROID_KEYSTORE_ALIAS --key-store-alias-password $ANDROID_KEYSTORE_ALIAS_PASSWORD
      --env.trec_env_staging
      --aab
      --copy-to appbundle.aab
  # Avoid this job if variable SKIP_STAGING is defined in the pipeline
  except:
    variables:
      - $SKIP_STAGING

# Build Android application for release and sign it using the Android keystore
Android Build Production:
  <<: *android_build_template
  stage: Package Production
  variables:
    <<: *android_variables
    BUILD_ENVIRONMENT: production
  script:
    - echo $ANDROID_KEYSTORE | base64 -di > /tmp/keystore # Copy keystore file in temp file
    - mv docs/google-config/production/google-services.json app/App_Resources/Android/google-services.json  || true # Replace Google services configuration with the "fbk-production" one
    - >-
      tns build android
      --bundle
      --env.uglify
      --env.aot
      --env.report
      --env.ci
      --env.sourceMap --env.hiddenSourceMap
      --release
      --key-store-path /tmp/keystore
      --key-store-password $ANDROID_KEYSTORE_PASSWORD --key-store-alias $ANDROID_KEYSTORE_ALIAS --key-store-alias-password $ANDROID_KEYSTORE_ALIAS_PASSWORD
      --env.trec_env_production
      --aab
      --copy-to appbundle.aab
  # The release in Production track must be triggered manually
  when: manual
  allow_failure: false
